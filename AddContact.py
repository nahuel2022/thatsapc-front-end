# -*- coding: utf-8 -*-
from PyQt4 import QtGui
from UiAddContact import Ui_AddContactDialog

class AddContact(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self);

        self.ui = Ui_AddContactDialog();
        self.ui.setupUi(self);
    
    def exec_(self):
        if (QtGui.QDialog.exec_(self)):
            return {"nick" : str(self.ui.Nick.text()), "id" : str(self.ui.Phone.text())}
        else:
            return None
