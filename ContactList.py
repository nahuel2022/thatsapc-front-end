from PyQt4 import QtGui, QtCore

ROLE = 1000

class ContactList(QtGui.QListWidget):
    def __init__(self,parent):
        QtGui.QListWidget.__init__(self,parent)
        self.__contacts = {}
    
    '''
        This method lets you to add, update or delete contacts.
        
        Contact should give this information:
        'id'   : phone number (including country code)
        'nick' : a nick to show in list (optional if you want to remove)
        'remove' : boolean, if true, contact with 'id' will be removed.
    '''
    def updateContact(self,contact):
        if (contact['remove']):
            for i in range(self.count()):
                data = self.item(i).data(ROLE).toString()
                if (data == contact['id']):
                    self.takeItem(i)
                    break
        else:
            index = -1
            for i in range(self.count()):
                data = self.item(i).data(ROLE).toString()
                if (data == contact['id']):
                    index = i
            if (index == -1):
                data = QtCore.QVariant(contact['id'])
                item = QtGui.QListWidgetItem(contact['nick'],self)
                item.setData(ROLE,data)
                self.addItem(item)
            else:
                self.item(i).setText(contact['nick'])
            self.__contacts[contact['id']] = contact
    
        



